import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Close from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";
import Button from "components/CustomButtons/Button.jsx";
import modalStyle from "assets/jss/material-dashboard-react/modalStyle.jsx";

function Transition(props) {
  return <Slide direction="down" {...props
  }
  />;
}

function Modal({ ...props }) {
  let { open, classes, closeModal, onClick, children} = props;
  return (
    <Dialog
      classes={{
        root: classes.center,
        paper: classes.modal
      }}
      open={open}
      onClose={closeModal}
      TransitionComponent={Transition}
      keepMounted
      aria-labelledby="alert-slide-title"
      aria-describedby="alert-slide-description"
    >
      <DialogTitle
        id="classic-modal-slide-title"
        disableTypography
        className={classes.modalHeader}
      >
        <IconButton
          className={classes.modalCloseButton}
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={() => closeModal()}>
          <Close className={classes.modalClose} />
        </IconButton>
      </DialogTitle>
      <DialogContent
        id="modal-slide-description"
        className={classes.modalBody}
      >
        {children}
      </DialogContent>
      <DialogActions
        className={classes.modalFooter + " " + classes.modalFooterCenter}
      >
        <Button onClick={closeModal} color="primary">
          TIDAK
          </Button>
        <Button onClick={onClick} color="primary" autoFocus>
          YA
          </Button>
      </DialogActions>
    </Dialog>
  );
}

Modal.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(modalStyle)(Modal);
