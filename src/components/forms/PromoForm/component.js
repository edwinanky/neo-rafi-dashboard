import React from "react";
import PropTypes from "prop-types";
import { Field } from 'redux-form';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "components/TextField/TextField.jsx";
import DropzoneInput from "components/DropzoneInput/DropzoneInput.jsx";
import SelectField from 'components/SelectField';
import moment from 'moment';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      image: null,
      cdn: null,
      selectedEnabled: "asiangames",
    };
    this.handleChangeEnabled = this.handleChangeEnabled.bind(this);
  }
  categoryOptions = [
    { label: 'Top Promo', value: 'BANNER' },
    { label: 'Promo Telkom Group', value: 'NON_BENCANA' },
    { label: 'Promo Kuliner', value: 'MERCHANT' },
  ]

  componentDidMount() {
    if (this.props.data !== null || this.props.data !== undefined) {
      this.props.initialize(this.props.data);
      console.log(this.props.data)
      this.setState({image: this.props.data.listNewsPromoImage[0], cdn: this.props.data.listNewsPromoCdn[0]})
    }
  }

  handleChangeEnabled(event) {
    this.setState({selectedEnabled:event.target.value})
    console.log(event)
  }

  onUpload (data) {
    let { onUpload } = this.props;

    let file = data[0]
    let reader = new FileReader();
    reader.onload = (event) => {
      this.setState({image:event.target.result});
      onUpload(event.target.result);
    };
    reader.readAsDataURL(file);
  }

  render() {
    let { classes, handleSubmit, label } = this.props;

    return (
      <form onSubmit={handleSubmit} className={classes.form}>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>{label}</h4>
        </CardHeader>
        <CardBody>
          <Grid container>
            <GridItem xs={12} sm={12} md={12}>
              <Field 
                name="listNewsPromoName"
                component={TextField}
                labelText="Title..."
                id="listNewsPromoName"
                formControlProps={{
                  fullWidth: true
                }}
                type= "text"
              />
            </GridItem>
          </Grid>
          <Grid container>
            <GridItem xs={12} sm={12} md={12}>
              <Field
                name="listNewsPromoDescription"
                component={TextField}
                labelText="Description"
                id="listNewsPromoDescription"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  multiline: true,
                  rows: 5
                }}
                type = "text"
              />
            </GridItem>
          </Grid>
          <Grid container>
            <GridItem xs={12} sm={12} md={6}>
              <Field
                component={SelectField}
                label="category"
                name="listNewsCategory"
                options={this.categoryOptions}
                type="text"
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Field 
                name = "listNewsPromoUrl"
                component={TextField}
                labelText="URL"
                id = "listNewsPromoUrl"
                formControlProps={{
                  fullWidth: true
                }}
                type= "text"
              />
            </GridItem>
          </Grid>
          <Grid container>
            <GridItem xs={12} sm={12} md={6}>
              <Field
                name="liststartDate"
                component={TextField}
                labelText="Start Date"
                id="liststartDate"
                formatDate={(date) => moment(date).format('YYYY-MM-DD')}
                formControlProps={{
                  fullWidth: true
                }}
                type= "date"
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <Field
                name="listoutDate"
                component={TextField}
                labelText="End Date"
                id="listoutDate"
                formatDate={(date) => moment(date).format('YYYY-MM-DD')}
                formControlProps={{
                  fullWidth: true
                }}
                type= "date"
              />
            </GridItem>
          </Grid>
          <Grid container>
            <GridItem xs={12} sm={12} md={6}>
              <h6 htmlFor={"listNewsPromoCdn"}>CDN</h6>
              <Field
                name="listNewsPromoCdn"
                component={DropzoneInput}
                onChange={(data) =>{this.onUpload(data)}}
              />
              {this.state.cdn !== null ? <img src={this.state.cdn} width="100%" alt="cdnPreview" /> : null}

            </GridItem>
            <GridItem xs={12} sm={12} md={6}>
              <h6 htmlFor={"listNewsPromoImage"}>Image</h6>
              <Field
                name="listNewsPromoImage"
                component={DropzoneInput}
                onChange={(data) =>{this.onUpload(data)}}
              />
              {this.state.image !== null ? <img src={this.state.image} width="100%" alt="newsPreview" /> : null}
            </GridItem>
          </Grid>
        </CardBody>
        <CardFooter className={classes.cardFooter}>
          <Button type="submit" simple color="primary" size="lg">
            Submit
          </Button>
        </CardFooter>
      </form>
    );
  }
}
Component.propTypes = {
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  submitting: PropTypes.bool,
  isLoading: PropTypes.bool,
  classes: PropTypes.object,
  data: PropTypes.object,
  label: PropTypes.string,
};