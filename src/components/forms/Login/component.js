import React from "react";
import PropTypes from "prop-types";
import { Field } from 'redux-form';
import InputAdornment from "@material-ui/core/InputAdornment";
import LockOutline from "@material-ui/icons/LockOutline";
import Person from "@material-ui/icons/Person";
import Button from "components/CustomButtons/Button.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "components/TextField/TextField.jsx";

export default class Component extends React.Component {

  render() {
    let { classes, handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit} className={classes.form}>
        <CardHeader color="danger" className={classes.cardHeader}>
          <h4>Teman Berbagi</h4>
        </CardHeader>
        <CardBody>
          <Field 
            name="username"
            component={TextField}
            labelText="Username..."
            id="username"
            formControlProps={{
              fullWidth: true
            }}
            type= "text"
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Person className={classes.inputIconsColor} />
                </InputAdornment>
              )
            }}
          />
          <Field
            name="password"
            component={TextField}
            labelText="Password"
            id="password"
            formControlProps={{
              fullWidth: true
            }}
            type = "password"
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <LockOutline
                    className={classes.inputIconsColor}
                  />
                </InputAdornment>
              )
            }}
          />
        </CardBody>
        <CardFooter className={classes.cardFooter}>
          <Button type="submit" simple color="primary" size="lg">
            Submit
          </Button>
        </CardFooter>
      </form>
    );
  }
}
Component.propTypes = {
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  submitting: PropTypes.bool,
  isLoading: PropTypes.bool,
  classes: PropTypes.object,
};