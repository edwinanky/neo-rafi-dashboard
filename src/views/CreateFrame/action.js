import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchFrameCreate(data) {
  return dispatch => {
    const options = {
      method: 'post',
      url: SERVICES.ADD_FRAME_LIST,
      data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(createOfFrameFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/frame';
      })
      .catch(() => {
        dispatch(createOfFrameFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}


function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function createOfFrameFetchedAction(data) {
  return {
    type: ACTIONS.CREATE_OF_FRAME_FETCHED,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}