import { ACTIONS } from 'constants/index';

const initialState = {
  data: []
};

export default function reducer(state = initialState, action) {
  const { CREATE_OF_FRAME_FETCHED } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case CREATE_OF_FRAME_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}
