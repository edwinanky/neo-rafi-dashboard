import { ACTIONS } from 'constants/index';

const initialState = {
  data: []
};

export default function reducer(state = initialState, action) {
  const { ADD_NEWS } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case ADD_NEWS:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}
