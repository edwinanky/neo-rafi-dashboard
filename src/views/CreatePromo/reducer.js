import { ACTIONS } from 'constants/index';

const initialState = {
  data: []
};

export default function reducer(state = initialState, action) {
  const { ADD_PROMO } = ACTIONS;
  const { type, data } = action;

  switch (type) {
    case ADD_PROMO:
      return {
        ...state,
        isLoading: false,
        data,
        type
      };
    default:
      return state;
  }
}
