import React from 'react';
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import NewsForm from "components/forms/NewsForm";
import { ACTIONS } from 'constants/index';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      image: null,
      data: null
    };
  }

  componentDidMount() {
    const { actions, match } = this.props;

    actions.fenchGetDetailNews(match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    let { DETAIL_OF_NEWS_FETCHED } = ACTIONS;
    let { type, data } = nextProps;
    if (type === DETAIL_OF_NEWS_FETCHED) {
      this.setState({render:true, data});
    }
  }

  _handleEditNews = (data) => {
    data = Object.assign(data, {newsImage:this.state.image})
    let { actions } = this.props;

    actions.fetchNewsEdit(data);
  }

  _handleUpload = (image) => {
    this.setState({image});
  }

  render() {
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            {this.state.render ?
              <NewsForm label={'Edit News'} onSubmit={(data) => { this._handleEditNews(data) }} data={this.state.data} onUpload={(file) => this._handleUpload(file)} />
              :
              <div>loading...</div>
            }
          </Card>
        </GridItem>
      </Grid>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};