import services from './services';
import routes from './routes';

export const ROUTES = routes;
export const SERVICES = services;
export const TOKEN_STORAGE = 'access_token';